ArchQtile README.md

@TODO
- [ ] reinstall arch so that the stupid stuff from anarchy doesn't happen again

Install on pacstrap
 - base
 - base-devel
 - linux
 - linux-firmware
 - vim

Install on chroot (pacman)
 - networkmanager
 - grub

High Priority
 - pcmanfm
 - noto-fonts / ttf-jetbrains-mono
 - xorg-server
 - qtile
 - alacritty
 - nm-connection-editor
 - pavucontrol

Item List:
 - xbindkeys
 - picom
 - exa
 - rofi
 - FM6000 ()
 - qalulate-gtk
 - lxappearance
 - chromium/firefox/both
 - feh
 - atom
 - xorg-server
 - betterlockscreen
