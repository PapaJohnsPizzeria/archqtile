#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

alias ls='exa -al --icons --header'
alias c='clear; source ~/.bashrc'
PS1='[\u@\h \W]\$ '

eval "$(starship init bash)"

fm6000 -dog -de "Qtile" -n -c bright_magenta
